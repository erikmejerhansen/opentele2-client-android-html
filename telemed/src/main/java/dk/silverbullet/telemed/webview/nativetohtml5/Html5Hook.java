package dk.silverbullet.telemed.webview.nativetohtml5;

import com.google.gson.Gson;

public class Html5Hook {

    // --*-- Fields --*--

    private String elementId;
    private String modelName;
    private String callbackName;

    // --*-- Constructors --*--

    public Html5Hook(String elementId, String modelName, String callbackName) {
        this.elementId = elementId;
        this.modelName = modelName;
        this.callbackName = callbackName;
    }

    // --*-- Methods --*--

    public static Html5Hook fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Html5Hook.class);
    }

    public String getElementId() {
        return elementId;
    }

    public String getModelName() {
        return modelName;
    }

    public String getCallbackName() {
        return callbackName;
    }
}
