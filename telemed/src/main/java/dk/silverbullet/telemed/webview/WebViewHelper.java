package dk.silverbullet.telemed.webview;

import android.annotation.SuppressLint;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.List;

import dk.silverbullet.telemed.webview.html5tonative.JavaScriptInterface;
import dk.silverbullet.telemed.webview.html5tonative.JsonEventHub;

/**
 * Takes a <code>WebView</code> and initializes it.
 *
 * @author Peter Urbak
 * @version 2015-04-28
 */
public class WebViewHelper {

    // --*-- Constructors --*--

    /**
     * Constructs <code>WebViewHelper</code>
     */
    public WebViewHelper() {
        // empty
    }

    // --*-- Methods --*--

    /**
     * Initializes a <code>WebView</code>
     */
    @SuppressLint("JavascriptInterface")
    public void initializeWebView(List<JavaScriptInterface> javaScriptInterfaces,
                                  WebView webView, JsonEventHub eventHub, AppLoaded loadedCallback) {

        EmbeddedWebViewClient client = new EmbeddedWebViewClient();
        client.setWebViewCallback(loadedCallback);
        webView.setWebViewClient(client);

        configureWebView(webView);
        configureDebugging(webView);
        for (JavaScriptInterface javaScriptInterface : javaScriptInterfaces) {
            webView.addJavascriptInterface(javaScriptInterface, javaScriptInterface.getName());
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void configureWebView(WebView webView) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.setInitialScale(1);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setBuiltInZoomControls(false);

        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
    }

    private void configureDebugging(WebView webView) {
        webView.setWebChromeClient(new WebChromeClient() {

            @SuppressWarnings("NullableProblems")
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("OpenTele:Console", cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId());
                return true;
            }
        });
    }

}
