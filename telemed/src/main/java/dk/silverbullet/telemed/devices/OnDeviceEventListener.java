package dk.silverbullet.telemed.devices;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.device_integration.listeners.OnEventListener;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.nativetohtml5.Html5Hook;
import dk.silverbullet.telemed.webview.Html5WebView;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

public class OnDeviceEventListener implements OnEventListener {

    private static final String TAG = Util.getTag(OnDeviceEventListener.class);

    private final WebViewMessageDispatcher webViewMessageDispatcher;
    private final String measurementType;

    public OnDeviceEventListener(WebViewMessageDispatcher webViewMessageDispatcher, String measurementType) {
        this.webViewMessageDispatcher = webViewMessageDispatcher;
        this.measurementType = measurementType;
    }

    @Override
    public void onEvent(JSONObject event) throws JSONException {
        Log.d(TAG, event.toString());

        JSONObject response = new JSONObject();
        response.put("messageType", "deviceMeasurementResponse");
        response.put("measurementType", measurementType);
        response.put("event", event);

        webViewMessageDispatcher.send(response);
    }

}
