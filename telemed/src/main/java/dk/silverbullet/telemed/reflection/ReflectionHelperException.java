package dk.silverbullet.telemed.reflection;

public class ReflectionHelperException extends Exception {

    // --*-- Constructors --*--

    public ReflectionHelperException(Exception e) {
        super(e);
    }

}
