package dk.silverbullet.device_integration.devices.noninsaturation.packet;

import java.io.IOException;

public interface PacketReceiver {

    void setSerialNumber(NoninSerialNumberPacket packet);

    void addMeasurement(NoninMeasurementPacket packet);

    void error(IOException e);

    void sendChangeDataFormatCommand();

    void sendChangeDataFormatCommand2();

}
