package dk.silverbullet.device_integration.devices.andweightscale.continua.packet.input;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.exceptions.UnexpectedPacketFormatException;
import dk.silverbullet.device_integration.devices.andweightscale.Weight;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;
import dk.silverbullet.device_integration.protocols.continua.packet.input.ConfirmedMeasurementDataPacket;
import dk.silverbullet.device_integration.protocols.continua.packet.input.OrderedByteReader;
import dk.silverbullet.device_integration.devices.andweightscale.Weight.Unit;

public class AndWeightScaleConfirmedMeasurementDataPacket extends ConfirmedMeasurementDataPacket {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(AndWeightScaleConfirmedMeasurementDataPacket.class);

    private static final int NUMBER_OF_DECIMALS_1 = 0xFF00;
    private static final int NUMBER_OF_DECIMALS_2 = 0xFE00;
    private static final int MDC_DIM_KILO_G = 0x06C3;
    private static final int MDC_DIM_LB = 0x06E0;
    private static final int A_AND_D_MESSAGE = 101;
    private static final long NO_TIME = -1;

    private long newestTimestamp;

    private Unit unit;
    private Weight newestWeight;
    private DeviceInformation deviceInformation;

    // --*-- Constructors --*--

    public AndWeightScaleConfirmedMeasurementDataPacket(byte[] contents) throws IOException {
        super(contents);
    }

    // --*-- Methods --*--

    @Override
    protected void readObjects(int eventType, OrderedByteReader in) throws IOException {

        switch (eventType) {
            case MDC_ATTRIBUTES:
                readMedicalDeviceSystemAttributes(in);
                break;
            case MDC_NOTI_SCAN_REPORT_VAR:
                readOldProtocolWeightMeasurement(in);
                break;
            case MDC_NOTI_CONFIG:
                readNewProtocolConfigurationMeasurement(in);
                break;
            case MDC_NOTI_SCAN_REPORT_MP_FIXED:
                readNewProtocolWeightMeasurement(in);
                break;
            default:
                throw new UnexpectedPacketFormatException("Unexpected event-type 0x" +
                        Integer.toString(eventType, 16) +
                        " (expected 0x0D1E or 0x0D1F)");
        }
    }

    private void readMedicalDeviceSystemAttributes(OrderedByteReader in) throws IOException {
        Log.i(TAG, "readMedicalDeviceSystemAttributes");

        checkShort(in, "attribute-list.count = 10", 0x000A);
        int deviceInformationLength = in.readShort(); // attribute-list.length = 178
        if (deviceInformationLength != 178) {
            // TODO: More dynamic parsing of MDS Attributes response
            Log.d(TAG, "Unknown deviceInformation, returning");
            return;
        }
        checkShort(in, "attribute-id = MDC_ATTR_SYS_TYPE_SPEC_LIST", 0x0A5A);
        checkShort(in, "attribute-value.length = 8", 0x0008);
        checkShort(in, "TypeVerList.count = 1", 0x0001);
        checkShort(in, "TypeVerList.length = 4", 0x0004);
        checkShort(in, "type = MDC_DEV_SPEC_PROFILE_SCALE", 0x100F);
        checkShort(in, "version = version 1 of the specialization", 0x0001);
        checkShort(in, "attribute-id = MDC_ATTR_ID_MODEL", 0x0928);
        checkShort(in, "attribute-value.length = 28", 0x001C);
        int manufacturerLength = in.readShort(); // string length = 12
        String manufacturer = readString(in, manufacturerLength-1);
        in.readByte();
        int modelLength = in.readShort(); // "string length = 12
        String model = readString(in, modelLength);
        checkShort(in, "attribute-id = MDC_ATTR_SYS_ID", 0x0984);
        checkShort(in, "attribute-value.length = 10", 0x000A);
        checkShort(in, "octet string length = 8", 0x0008);
        String eui64 = String.valueOf(in.readLong());
        checkShort(in, "attribute-id = MDC_ATTR_DEV_CONFIG_ID", 0x0A44);
        checkShort(in, "attribute-value.length = 2", 0x0002);
        checkShort(in, "dev-config-id = 16384 (extended-config-start)", 0x4000);
        checkShort(in, "attribute-id = MDC_ATTR_ID_PROD_SPECN", 0x092D);
        checkShort(in, "attribute-value.length = 40", 0x0028);
        checkShort(in, "ProductionSpec.count = 2", 0x0002);
        checkShort(in, "ProductionSpec.length = 36", 0x0024);
        checkShort(in, "ProdSpecEntry.spec-type = 1 (serial-number)", 0x0001);
        checkShort(in, "ProdSpecEntry.component-id = 0", 0x0000);
        int serialNumberLength = in.readShort(); //string length = 10
        String serialNumber = readString(in, serialNumberLength);
        checkShort(in, "ProdSpecEntry.spec-type = 5 (fw-revision)", 0x0005);
        checkShort(in, "ProdSpecEntry.component-id = 0", 0x0000);
        int firmwareRevisionLength = in.readShort(); // string length = 14
        String firmwareRevision = readString(in, firmwareRevisionLength);
        checkShort(in, "attribute-id = MDC_ATTR_TIME_ABS", 0x0987);
        checkShort(in, "attribute-value.length = 8", 0x0008);
        readTimestamp(in);
        checkShort(in, "attribute-id = MDC_ATTR_REG_CERT_DATA_LIST", 0x0A4B);
        checkShort(in, "attribute-value.length = 22", 0x0016);
        checkShort(in, "auth-body-continua", 0x0002);
        checkShort(in, "??", 0x0012);
        checkShort(in, "??", 0x0201);
        checkShort(in, "??", 0x0008);
        checkShort(in, "??", 0x0105);
        checkShort(in, "??", 0x0001);
        checkShort(in, "??", 0x0002);
        checkShort(in, "??", 0x400F);
        checkShort(in, "??", 0x0202);
        checkShort(in, "??", 0x0002);
        checkShort(in, "??", 0x0000);
        checkShort(in, "attribute-id = MDC_ATTR_MDS_TIME_INFO", 0x0A45);
        checkShort(in, "attribute-value.length = 16", 0x0010);
        checkShort(in, "??", 0xC010);
        checkShort(in, "??", 0x1F00);
        checkShort(in, "??", 0xFFFF);
        checkShort(in, "??", 0xFFFF);
        checkShort(in, "??", 0x0064);
        checkShort(in, "??", 0x0000);
        checkShort(in, "??", 0x0000);
        checkShort(in, "??", 0x0000);
        checkShort(in, "attribute-id = MDC_ATTR_POWER_STAT", 0x0955);
        checkShort(in, "attribute-value.length = 2", 0x0002);
        checkShort(in, "dev-config-id = 16384 (extended-config-start)", 0x4000);
        checkShort(in, "attribute-id = MDC_ATTR_UNIT_CODE", 0x099C);
        checkShort(in, "attribute-value.length = 2", 0x0002);
        checkShort(in, "??", 0x0064);

        deviceInformation = new DeviceInformation(model, eui64,
                firmwareRevision, manufacturer, serialNumber, null);
        Log.d(TAG, "Parsed deviceInformation: " + deviceInformation);

    }

    private void readOldProtocolWeightMeasurement(OrderedByteReader in) throws IOException {
        Log.i(TAG, "readOldProtocolWeightMeasurement");

        newestTimestamp = NO_TIME;

        int numberOfMeasurements = in.readShort(); // ScanReportInfoFixed.obs-scan-fixed.count
        in.readShort(); // ScanReportInfoFixed.obs-scan-fixed.length
        for (int i = 0; i < numberOfMeasurements; i++) {

            try {
                int objectHandle = in.readShort();
                if (objectHandle == A_AND_D_MESSAGE) {
                    Log.d(TAG, "Found A&D custom message from new protocol, returning");
                    return;
                }

                if (objectHandle != 0x0001) {
                    throw new UnexpectedPacketFormatException("Unexpected object" +
                            "-handle (" + objectHandle + " - expected 1)");
                }

                checkShort(in, "Number of attributes?", 0x0003);
                checkShort(in, "Length of measurement", 0x001A);
                checkShort(in, "??", 0x0A56);

                checkShort(in, "Length of nu-observed-value", 0x0004);
                float weight = readWeight(in);

                checkShort(in, "MDC_ATTR_TIME_STAMP_ABS", 0x0990);
                checkShort(in, "Length of timestamp", 0x0008);
                long timestamp = readTimestamp(in);

                checkShort(in, "MDC_ATTR_UNIT_CODE", 0x0996);
                checkShort(in, "Length of unit code", 0x0002);
                Unit unit = readUnit(in);

                if (timestamp > newestTimestamp) {
                    newestWeight = new Weight(weight, unit);
                    newestTimestamp = timestamp;
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private void readNewProtocolConfigurationMeasurement(OrderedByteReader in) throws IOException {
        Log.i(TAG, "readNewProtocolConfigurationMeasurement");

        // Below we check that we receive the expected configuration object,
        // ideally we should a new parser from the configuration object to parse
        // the subsequent weight measurements.

        checkShort(in, "Number of measurements configuration", 0x0002); // config-obj-list.count = 2
        checkShort(in, "Length of config message", 0x004F); // config-obj-list.length = 79

        // First measurement configuration
        checkShort(in, "object class", 0x0006); // obj-class = MDC_MOC_VMO_METRIC_NU
        checkShort(in, "obj handle", 0x0001); // obj-handle = 1 (-> 1st measurement is body weight)
        checkShort(in, "attributes count", 0x0004); // attributes.count = 4
        checkShort(in, "attributes length", 0x0024); // attributes.length = 36
        checkShort(in, "attribute id", 0x092F); // attribute-id = MDC_ATTR_ID_TYPE
        checkShort(in, "attribute value length", 0x0004); // attribute-value.length = 4
        checkShort(in, "mass body actual #1", 0x0002); // MDC_MASS_BODY_ACTUAL
        checkShort(in, "mass body actual #2", 0xE140); // MDC_MASS_BODY_ACTUAL
        checkShort(in, "attribute id", 0x0A46); // attribute-id = MDC_ATTR_METRIC_SPEC_SMALL
        checkShort(in, "attribute value length", 0x0002); // attribute-value.length = 2
        checkShort(in, "fluff", 0xF040); // intermittent, Stored data, upd & msmst aperiodic, agent init, measured
        checkShort(in, "attribute id", 0x0996); // attribute-id = MDC_ATTR_UNIT_CODE
        checkShort(in, "attribute value length", 0x0002); // 0x00 0x02 attribute-value.length = 2
        unit = readUnit(in); // MDC_DIM_KILO_G | MDC_DIM_LB
        newestWeight = new Weight(-1, unit); // we sent the unit before the value
        Log.d(TAG, "Unit set to: " + unit);
        checkShort(in, "attribute id", 0x0A55); // attribute-id = MDC_ATTR_ATTRIBUTE_VALUE_MAP
        checkShort(in, "attribute value length", 0x000C); // attribute-value.length = 12
        checkShort(in, "attribute value map count", 0x0002); // AttrValMap.count = 2
        checkShort(in, "attribute value map length", 0x0008); // AttrValMap.length = 8
        checkShort(in, "attribute value obs simp?", 0x0A56); // MDC_ATTR_NU_VAL_OBS_SIMP
        checkShort(in, "value length", 0x0004); // value length = 4
        checkShort(in, "attribute time stamp absolute", 0x0990); // MDC_ATTR_TIME_STAMP_ABS
        checkShort(in, "value length", 0x0008); // value length = 8

        // Second measurement configuration
        checkShort(in, "object class", 0x0006); // obj-class = MDC_MOC_VMO_METRIC_NU
        checkShort(in, "obj handle", 0x0065); // obj-handle = 101 (-> 2nd measurement is A&D specific)
        checkShort(in, "attribute count", 0x0004); // attributes.count = 4
        checkShort(in, "attribute length", 0x001B); // attributes.length = 27
        // We stop reading the object here, since we do not really care about its content.

        Log.d(TAG, "Successfully read configuration measurement");
    }

    private void readNewProtocolWeightMeasurement(OrderedByteReader in) throws IOException {
        Log.i(TAG, "readNewProtocolWeightMeasurement");
        newestTimestamp = NO_TIME;

        int numberOfMeasurements = in.readShort(); // ScanReportInfoFixed.obs-scan-fixed.count

        for (int i = 0; i < numberOfMeasurements; i++) {

            Log.d(TAG, "Measurements length: " + in.readShort()); // ScanReportInfoFixed.obs-scan-fixed.length
            Log.d(TAG, "Maybe object handle?: " + in.readShort()); // TODO: Check with the configuration above and the received bytes what these guys should be
            in.readShort();
            in.readShort();
            checkShort(in, "Object handle", 0x01);
            checkShort(in, "Length", 0x0C);
            float weight = readWeight(in);
            Log.d(TAG, "weight: " + weight);
            long timestamp = readTimestamp(in); // TODO: Check that the timestamp is what we expect it to be!
            Log.d(TAG, "timestamp: " + timestamp);

            if (timestamp > newestTimestamp) {
                newestWeight = new Weight(weight, null); // we have already sent the unit
                newestTimestamp = timestamp;
            }

        }
    }

    private float readWeight(OrderedByteReader in) throws IOException {
        float divisionFactor = readDivisionFactor(in);
        int weightInSomeUnit = in.readShort() & 0xFFFF /* Make unsigned */;
        return weightInSomeUnit / divisionFactor;
    }

    private float readDivisionFactor(OrderedByteReader in) throws IOException {
        int baseCode = in.readShort();
        if (baseCode == NUMBER_OF_DECIMALS_1) {
            return 10;
        }
        if (baseCode == NUMBER_OF_DECIMALS_2) {
            return 100;
        }
        throw new UnexpectedPacketFormatException("Unexpected base code 0x" +
                Integer.toString(baseCode, 16) + "(expected 0xFF00 or 0xFE00)");
    }

    private long readTimestamp(OrderedByteReader in) throws IOException {
        return in.readLong();
    }

    private String readString(OrderedByteReader in, int length) throws IOException {
        StringBuilder stringBuilder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            stringBuilder.append((char) in.readByte());
        }
        return stringBuilder.toString();
    }

    private Unit readUnit(OrderedByteReader in) throws IOException {
        int unitCode = in.readShort();
        if (unitCode == MDC_DIM_KILO_G) {
            return Unit.KG;
        }
        if (unitCode == MDC_DIM_LB) {
            return Unit.LBS;
        }
        throw new UnexpectedPacketFormatException("Unexpected unit code 0x" +
                Integer.toString(unitCode, 16) + "(expected 0x06C3 or 0x06E0)");
    }

    @Override
    public String toString() {
        return "AndWeightScaleConfirmedMeasurementDataPacket" +
                " [newestTimestamp=0x" + Long.toString(newestTimestamp, 16) +
                ", newestWeight=" + newestWeight +
                ", deviceInformation=" + deviceInformation +
                "]";
    }

    // -*- Getters -*-

    public Weight getWeight() {
        return newestWeight;
    }

    public long getTimestamp() {
        return newestTimestamp;
    }

    public boolean hasDeviceInformation() {
        return  deviceInformation != null;
    }

    public DeviceInformation getDeviceInformation() {
        return deviceInformation;
    }

}
