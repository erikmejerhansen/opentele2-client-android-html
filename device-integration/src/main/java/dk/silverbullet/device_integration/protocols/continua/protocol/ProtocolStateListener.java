package dk.silverbullet.device_integration.protocols.continua.protocol;

import java.io.IOException;

import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;
import dk.silverbullet.device_integration.protocols.continua.packet.output.OutputPacket;

public interface ProtocolStateListener<MeasurementType> {

    void sendPacket(OutputPacket packet) throws IOException;

    void tooManyRetries();

    void finish();

    void finishNow();

    void measurementReceived(DeviceInformation deviceInformation, MeasurementType measurement);

    void noMeasurementsReceived();
}
