package dk.silverbullet.device_integration.protocols.continua.packet.input;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.exceptions.UnexpectedPacketFormatException;
import dk.silverbullet.device_integration.protocols.continua.ContinuaPacketTag;

public abstract class InputPacket {

    private static final String TAG = Util.getTag(InputPacket.class);

    private final ContinuaPacketTag tag;
    final byte[] contents;

    protected InputPacket(ContinuaPacketTag tag, byte[] contents) {
        this.tag = tag;
        this.contents = contents;

        Log.d(TAG, "Tag: " + tag);
        String byteString = "";
        for (byte content : contents) {
            byteString += String.format("%02X", content) + ", ";
        }
        Log.d(TAG, "Contents: " + byteString);
    }

    public ContinuaPacketTag getTag() {
        return tag;
    }

    public byte[] getContents() {
        return contents;
    }

    public int length() {
        return contents.length;
    }

    protected void checkShort(OrderedByteReader in, String name, int expected) throws IOException,
            UnexpectedPacketFormatException {
        int value = in.readShort();
        if (value != expected) {
            throw new UnexpectedPacketFormatException("Unexpected " + name + "(" + value + " - expected " + expected
                    + ")");
        }
    }

    protected void checkByte(OrderedByteReader in, String name, int expected) throws IOException,
            UnexpectedPacketFormatException {
        int value = in.readByte();
        if (value != expected) {
            throw new UnexpectedPacketFormatException("Unexpected " + name + "(" + value + " - expected " + expected
                    + ")");
        }
    }

    protected void checkInt(OrderedByteReader in, String name, int expected) throws IOException,
            UnexpectedPacketFormatException {
        int value = in.readInt();
        if (value != expected) {
            throw new UnexpectedPacketFormatException("Unexpected " + name + "(" + value + " - expected " + expected
                    + ")");
        }
    }
}
