package dk.silverbullet.device_integration.listeners;

public class JSONConstants {

    // --*-- Constants --*--

    // Generic
    public static final String TIMESTAMP = "timestamp";
    public static final String DEVICE = "device";
    public static final String TYPE = "type";
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    public static final String INFO = "info";
    public static final String CONNECTING = "CONNECTING";
    public static final String CONNECTED = "CONNECTED";
    public static final String DISCONNECTED = "DISCONNECTED";
    public static final String TEMPORARY_PROBLEM = "TEMPORARY_PROBLEM";
    public static final String PERMANENT_PROBLEM = "PERMANENT_PROBLEM";
    public static final String ERROR = "error";
    public static final String MEASUREMENT = "measurement";
    public static final String UNIT = "unit";
    public static final String VALUE = "value";

    // Device
    public static final String MODEL = "model";
    public static final String EUI_64 = "eui64";
    public static final String FIRMWARE_REVISION = "firmwareRevision";
    public static final String MANUFACTURER = "manufacturer";
    public static final String SERIAL_NUMBER = "serialNumber";
    public static final String SYSTEM_ID = "systemId";

    // Blood pressure
    public static final String BLOOD_PRESSURE = "blood pressure";
    public static final String SYSTOLIC = "systolic";
    public static final String DIASTOLIC = "diastolic";
    public static final String MEAN_ARTERIAL_PRESSURE = "meanArterialPressure";
    public static final String MM_HG = "mmHg";

    // Pulse
    public static final String PULSE = "pulse";
    public static final String BPM = "bpm";

    // Saturation
    public static final String SATURATION = "saturation";
    public static final String PERCENTAGE = "%";
    public static final String SATURATION_CONNECTING = "SATURATION_CONNECTING";
    public static final String SATURATION_CONNECTED = "SATURATION_CONNECTED";
    public static final String SATURATION_DISCONNECTED = "SATURATION_DISCONNECTED";
    public static final String SATURATION_COULD_NOT_CONNECT = "SATURATION_COULD_NOT_CONNECT";
    public static final String SATURATION_PERMANENT_PROBLEM = "SATURATION_PERMANENT_PROBLEM";
    public static final String SATURATION_TEMPORARY_PROBLEM = "SATURATION_TEMPORARY_PROBLEM";
    public static final String SATURATION_FIRST_TIMEOUT = "SATURATION_FIRST_TIMEOUT";
    public static final String SATURATION_FINAL_TIMEOUT = "SATURATION_FINAL_TIMEOUT";

    // Weight
    public static final String WEIGHT = "weight";
    public static final String WEIGHT_CHANGE_UNIT = "WEIGHT_CHANGE_UNIT";
    public static final String WEIGHT_CONNECTING = "WEIGHT_CONNECTING";

    // --*-- Constructors --*--

    private JSONConstants() {
        throw new RuntimeException("Constants cannot be instantiated.");
    }
}
